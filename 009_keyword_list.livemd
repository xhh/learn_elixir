# 9. 关键字列表 (Keyword list)

```elixir
import IEx.Helpers
```

## 示例

```elixir
Map.to_list(%{answer: 42, bot: "Marvin"})
```

```elixir
h(Keyword)
```

```elixir
# %{ :answer => 42, :bot => "Marvin" }
# %{  answer:   42,  bot:   "Marvin" }

# [ {:answer, 42}, {:bot, "Marvin"} ]
# [   answer: 42,    bot: "Marvin"  ]

[answer: 42, bot: "Marvin"] == [{:answer, 42}, {:bot, "Marvin"}]
```

因为是列表, 所以允许有重复的 key (元组的第一个元素), 而 Map 不会存在重复的 key

```elixir
[include: "*.h", include: "*.cc"]
```

## Keyword 模块常用函数

```elixir
k = [answer: 42, bot: "Marvin", answer: 999]
```

```elixir
Keyword.get(k, :answer)
```

```elixir
k[:answer]
```

```elixir
Keyword.get_values(k, :answer)
```

```elixir
Keyword.put(k, :aa, "bb")
```

```elixir
Keyword.delete(k, :answer)
```

```elixir
Keyword.merge(k, hello: :world, from: China)
```

## 省略方括号

http://elixir-lang.org/getting-started/keywords-and-maps.html#keyword-lists

<!-- livebook:{"force_markdown":true} -->

```elixir
String.split("1  2  3", " ", trim: true)
String.split("1  2  3", " ", [trim: true])
String.split("1  2  3", " ", [{:trim, true}])

query =
  from w in Weather,
    where: w.prcp > 0,
    where: w.temp < 20,
    select: w

query =
  from(w in Weather, [
    where: w.prcp > 0,
    where: w.temp < 20,
    select: w
  ])

query =
  from(w in Weather, [
    {:where, w.prcp > 0},
    {:where, w.temp < 20},
    {:select, w}
  ])
```

http://elixir-lang.org/getting-started/keywords-and-maps.html#do-blocks-and-keywords

<!-- livebook:{"force_markdown":true} -->

```elixir
  if true, do: "This will be seen", else: "This won't"
```

```elixir
if true do
  "This will be seen"
else
  "This won't"
end
```

```elixir
if true, do: "This will be seen", else: "This won't"
```

```elixir
if(true, do: "This will be seen", else: "This won't")
```

```elixir
if(true, [{:do, "This will be seen"}, {:else, "This won't"}])
```
