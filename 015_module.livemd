# 15. 模块 (Module)

```elixir
import IEx.Helpers
```

## Section

```elixir
defmodule AA.BB.CC do
  def ff do
    "hi"
  end
end
```

```elixir
defmodule AA.MyMod do
  def my_func do
    another_func()
  end

  def another_func do
    IO.puts(AA.BB.CC.ff())
    :ok
  end
end
```

```elixir
AA.MyMod.my_func()
```

```elixir
# lib/aa/bb/cc.ex
# lib/aa/my_mod.ex
```

```elixir
recompile()
```

```elixir
Hello.run()
```

```elixir
# IO.puts
```

```elixir
:程序.运行()
```

```elixir
# defstruct
# GenServer
```
