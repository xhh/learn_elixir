defmodule Hello do
  @moduledoc """
  这里是模块文档...
  """

  @spec run :: :ok
  @doc """
  这里是函数文档... Hello

  ## Examples

      iex> Hello.run()
      #=> 你好
      :ok

  """
  def run do
    IO.puts("你好!")
  end
end
